package diet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import diet.Order.OrderStatus;

/**
 * Represents a restaurant class with given opening times and a set of menus.
 */
public class Restaurant implements Comparable<Restaurant> {

	Food food;
	String name;
	ArrayList<WorkingHours> openingHours;
	HashMap<String, Menu> menus;
	Order[] orders = {};

	public Restaurant(Food f, String name) {
		this.food = f;
		this.name = name;
		openingHours = new ArrayList<>();
		menus = new HashMap<String, Menu>();
	}

	/**
	 * retrieves the name of the restaurant.
	 *
	 * @return name of the restaurant
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Define opening times. Accepts an array of strings (even number of elements)
	 * in the format {@code "HH:MM"}, so that the closing hours follow the opening
	 * hours (e.g., for a restaurant opened from 8:15 until 14:00 and from 19:00
	 * until 00:00, arguments would be {@code "08:15", "14:00", "19:00", "00:00"}).
	 *
	 * @param hm sequence of opening and closing times
	 */
	public void setHours(String... hm) {
		openingHours.clear();
		for(int i=0; i<hm.length/2; i++) {
			openingHours.add(new WorkingHours(hm[2*i], hm[2*i+1]));
		}
	}

	/**
	 * Checks whether the restaurant is open at the given time.
	 *
	 * @param time time to check
	 * @return {@code true} is the restaurant is open at that time
	 */
	public boolean isOpenAt(String time){
		return isOpenAt(new Time(time));
	}

	boolean isOpenAt(Time t) {
		for(WorkingHours w : openingHours) {
			if( w.includes(t) ) return true;
		}
		return false;
	}

	Time checkTime(Time t) {
		Collections.sort(openingHours);
		for(WorkingHours w : openingHours) {
			if( w.includes(t) ) return t;
		}
		for(WorkingHours w : openingHours) {
			if (w.getOpen().compareTo(t) > 0) {
				return w.getOpen();
			}
		}
		return openingHours.get(0).getOpen();
	}

	String convertToCorrectFormat(String time) {
		String[] parts = time.split(":");
		if (parts.length == 2) {
			String hour = parts[0].trim();
			String minute = parts[1].trim();

			if (hour.length() == 1) {
				hour = "0" + hour;
			}

			return hour + ":" + minute;
		}

		// If the time is already in the correct format or not in the expected format,
		// return it as is
		return time;
	}

	/**
	 * Adds a menu to the list of menus offered by the restaurant
	 *
	 * @param menu the menu
	 */
	public void addMenu(Menu menu) {
		this.menus.put(menu.getName(), menu);
	}

	/**
	 * Gets the restaurant menu with the given name
	 *
	 * @param name name of the required menu
	 * @return menu with the given name
	 */
	public Menu getMenu(String name) {
		return this.menus.get(name);
	}

	void addOrder(Order order) {
		orders = addItem(orders, order);
	}
	
	private static Order[] addItem(Order[] array, Order newOrder) {
        // Create a new array with increased size
		Order[] newArray = new Order[array.length + 1];

        // Copy the existing elements to the new array
        System.arraycopy(array, 0, newArray, 0, array.length);

        // Add the new item to the last position
        newArray[array.length] = newOrder;

        return newArray;
    }

	/**
	 * Retrieve all order with a given status with all the relative details in text
	 * format.
	 *
	 * @param status the status to be matched
	 * @return textual representation of orders
	 */
	public String ordersWithStatus(OrderStatus status) {
		
		StringBuilder b = new StringBuilder();
		Arrays.sort(orders, Comparator
				.comparing((Order o) -> o.restaurant.getName())
				.thenComparing((Order o) -> o.customer.toString())
				.thenComparing((Order o) -> o.delivery_time));
		for (Order o: orders) {
			if (o.getStatus() == status){
				b.append(o);
			}
		}
		return b.toString();
	}
	
	@Override
	public int compareTo(Restaurant o) {
		return this.getName().compareTo(o.getName());
	}

}
